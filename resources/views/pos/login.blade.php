@extends('pos.layouts.plain_layout')

@section('content')
<br>
<div class="row">
	<div class="large-4 columns">&nbsp;</div>
	<div class="large-4 columns">
		<div class="callout secondary">
			<form action="" method="post">
				
				<div class="row">
					<div class="large-12 columns">
						<div class="input-group">
							<span class="input-group-label"><i class="fa fa-lg fa-user"></i></span>
							<input class="input-group-field" placeholder="Username" type="text" id="username" name="username">
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="large-12 columns">
						<div class="input-group">
							<span class="input-group-label"><i class="fa fa-lg fa-lock"></i></span>
							<input class="input-group-field" placeholder="Password" type="password" id="password" name="password">
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="large-12 columns">
						<button class="button expanded" type="submit"><i class="fa fa-lg fa-sign-in"></i> Login</button>
					</div>
					<!--div class="large-6 columns">&nbsp;</div-->
				</div>

			</form>
		</div>
	</div>
	<div class="large-4 columns">&nbsp;</div>
</div>

@endsection