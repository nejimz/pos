<head>
    <title>POS</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation-flex.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

    <script type="text/javascript" src="{{ asset('js/jquery-1.12.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/foundation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>
</head>