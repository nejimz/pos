<!DOCTYPE html>
<html>
    @include('pos.layouts.plain_header')
    <body>
	    <div class="row">
	    	<div class="large-12 columns" role="content">
	    		@yield('content')
	    	</div>
	    </div>
    	@include('pos.layouts.plain_footer')
    </body>
</html>