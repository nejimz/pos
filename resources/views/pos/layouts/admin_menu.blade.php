<div class="top-bar">

  <div class="top-bar-title">
    <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
      <span class="menu-icon dark" data-toggle></span>
    </span>
    <strong>POS</strong>
  </div>

  <div id="responsive-menu">

    <div class="top-bar-left">
      <ul class="dropdown menu" data-dropdown-menu>
        <li class="active">
          <a href="#">Home</a>
        </li>
        <li>
          <a href="#">Customers</a>
          <ul class="menu vertical">
            <li><a href="#">Add</a></li>
          </ul>
        </li>
        <li>
          <a href="#">Suppliers</a>
          <ul class="menu vertical">
            <li><a href="#">Add</a></li>
          </ul>
        </li>
        <li>
          <a href="#">Items</a>
          <ul class="menu vertical">
            <li><a href="#">Add</a></li>
            <!--li>
              <a href="#">Kit</a>
              <ul class="menu vertical">
                <li><a href="#">Add</a></li>
              </ul>
            </li-->
          </ul>
        </li>
        <li>
          <a href="#">Item Kits</a>
          <ul class="menu vertical">
            <li><a href="#">Add</a></li>
          </ul>
        </li>
        <li>
          <a href="#">Reports</a>
        </li>
        <li>
          <a href="#">Sales</a>
        </li>
        <li>
          <a href="#">User</a>
          <ul class="menu vertical">
            <li><a href="#">Add</a></li>
          </ul>
        </li>
      </ul>
    </div>

    <!--div class="top-bar-right">
      <ul class="menu">
        <li><input type="search" placeholder="Search"></li>
        <li><button type="button" class="button">Search</button></li>
      </ul>
    </div-->

  </div>

</div>