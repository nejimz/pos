<!DOCTYPE html>
<html>
    @include('pos.layouts.admin_header')
    <body>
    	@include('pos.layouts.admin_menu')
	    <div class="row">
	    	<div class="large-12 columns">
	    		@yield('content')
	    	</div>
	    </div>
    	@include('pos.layouts.admin_footer')
    </body>
</html>